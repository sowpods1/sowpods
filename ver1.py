# initialisation
def access_words(4letters):
    with open("4letters.txt", 'r') as file:
        return {line.strip() for line in file if len(line.strip()) == 4}

def letter_difference(word1, word2):
    return sum(a != b for a, b in zip(word1, word2)) == 1

