def parse_line(line: str) -> list:
    words = line.strip().split()
    return [words]
def collect_data():
    return ([parse_line(line) for line in open("4letters.txt")])
