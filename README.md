# sowpods
## Word Transformation Sequence

This Python script finds the shortest transformation sequence from a start word to an end word, where each word in the sequence differs by exactly one letter from the previous word. The words used for transformations are read from a `words.txt` file containing a list of valid four-letter words.
